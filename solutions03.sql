

CREATE DATABASE blog_db;


CREATE TABLE Users (
    id INT NOT NULL AUTO_INCREMENT,
    Email VARCHAR(50) NOT NULL,
    Password VARCHAR(50) NOT NULL,
    Datetime_Created DATETIME NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE Posts (
    id INT NOT NULL AUTO_INCREMENT,
    User_id INT NOT NULL,
    Title VARCHAR(50) NOT NULL,
    Content VARCHAR(250) NOT NULL,
    Datetime_Posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_Posts_User_id
        FOREIGN KEY (User_id) REFERENCES Users(id)
        ON UPDATE CASCADE 
        ON DELETE RESTRICT
);




INSERT INTO Users (Email, Password, Datetime_Created) VALUES 
    ("johnsmith@gmail.com", "passwordA", '2021-01-01 1:00:00'),
    ("juandelacruz@gmail.com", "passwordB", '2021-01-01 2:00:00 '),
    ("janesmith@gmail.com", "passwordC", '2021-01-01 3:00:00' ),
    ("mariadelacruz@gmail.com", "passwordD", '2021-01-01 4:00:00' ),
    ("johndoe@gmail.com", "passwordE", '2021-01-01 1:00:00 ');



INSERT INTO Posts (User_id, Title, Content, Datetime_Posted) VALUES 
    (1, "First Code", "Hello World!", '2021-01-02 1:00:00'),
    (1,"Second Code", "Hello Earth!", '2021-01-02 2:00:00 '),
    (2,"Third Code", "Welcome to Mars!", '2021-01-02 3:00:00' ),
    (4,"Fourth Code", "Bye bye solar system!", '2021-01-02 4:00:00' )
   ;


SELECT * FROM posts WHERE User_id = 1;

SELECT Email, Datetime_Created FROM users;


UPDATE posts SET Content = "Hello to the people of the earth" WHERE id = 2;

DELETE FROM users WHERE email = "johndoe@gmail.com";